package com.example.usyasha69.simpleanimation.test;

import java.util.ArrayList;
import java.util.Collections;

public class Mock {

    public static ArrayList<String> getWords() {
        ArrayList<String> result = new ArrayList<>();

        String[] words = {"cloud", "feet", "legs", "bread", "grass", "boat", "butterfly",
                "square", "dragon", "ball", "coat", "chicken", "blanket", "cupcake", "bunny",
                "beach", "snake", "head", "heart", "jacket", "inchworm", "candle", "jellyfish", "bone",
                "hamburger", "smile", "truck", "spider", "doll", "lion", "airplane", "lips",
                "waist", "cell phone", "spaceship", "towel", "coin", "trash can",
                "stump", "electricity", "manatee", "lighthouse", "cheek", "cheeseburger", "tent",
                "pinwheel", "pirate", "doghouse", "batteries", "desk", "trumpet", "pineapple", "rug",
                "yo-yo", "treasure", "owl", "thief", "quilt", "pen", "skunk", "tire", "lake", "rose",
                "cabin", "honk", "season", "wind", "CD", "lie", "sponge", "lace",
                "handle", "bald", "yolk", "rind", "punk", "husband", "mold", "shampoo", "zoo", "chef",
                "birthday", "taxi", "nightmare", "yardstick", "runt", "comfy", "internet", "lap", "exercise",
                "koala", "diagonal", "fireside", "juggle", "rubber", "jazz", "kneel", "dent"};

        Collections.addAll(result, words);

        return result;
    }
}
