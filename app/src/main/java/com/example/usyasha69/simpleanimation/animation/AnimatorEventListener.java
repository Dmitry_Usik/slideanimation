package com.example.usyasha69.simpleanimation.animation;

import android.animation.Animator;
import android.view.View;

public class AnimatorEventListener implements Animator.AnimatorListener {

    private View view;

    public AnimatorEventListener(View view) {
        this.view = view;
    }

    @Override
    public void onAnimationStart(Animator animation) {

    }

    @Override
    public void onAnimationEnd(Animator animation) {
        view.clearAnimation();
        onEnd();
    }

    @Override
    public void onAnimationCancel(Animator animation) {

    }

    @Override
    public void onAnimationRepeat(Animator animation) {

    }

    public void onEnd() {

    }
}
