package com.example.usyasha69.simpleanimation.ui.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.usyasha69.simpleanimation.R;
import com.example.usyasha69.simpleanimation.adapter.WordsStatusRVAdapter;
import com.example.usyasha69.simpleanimation.model.TeamResultModel;
import com.example.usyasha69.simpleanimation.ui.activity.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ResultFragment extends Fragment {
    private static final String COMMAND_RESULT_MODEL_BUNDLE_KEY = "command_result_model";

    @BindView(R.id.fr_command_name)
    TextView commandName;
    @BindView(R.id.fr_command_score)
    TextView commandScore;
    @BindView(R.id.fr_words_status_rv)
    RecyclerView wordsStatus;

    private TeamResultModel teamResultModel;

    public static ResultFragment newInstance(TeamResultModel teamResultModel) {
        Bundle args = new Bundle();
        args.putParcelable(COMMAND_RESULT_MODEL_BUNDLE_KEY, teamResultModel);

        ResultFragment fragment = new ResultFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();

        if (args != null && args.containsKey(COMMAND_RESULT_MODEL_BUNDLE_KEY)) {
            teamResultModel = args.getParcelable(COMMAND_RESULT_MODEL_BUNDLE_KEY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_result, container, false);
        ButterKnife.bind(this, root);

        commandName.setText(teamResultModel.name);

        int scoreValue = teamResultModel.score;
        String score;

        if (scoreValue > 0 ) {
            score = "+" + scoreValue;
        } else {
            score = String.valueOf(scoreValue);
        }

        commandScore.setText(score);

        wordsStatus.setLayoutManager(new LinearLayoutManager(getContext()));
        wordsStatus.setAdapter(new WordsStatusRVAdapter(getContext(), teamResultModel.wordsStatus));

        return root;
    }

    @OnClick(R.id.fr_continue)
    public void onClickContinue(View v) {
        ((MainActivity) getActivity()).createPreparatoryFragment();
    }
}
