package com.example.usyasha69.simpleanimation.ui.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.usyasha69.simpleanimation.R;
import com.example.usyasha69.simpleanimation.model.TeamModel;
import com.example.usyasha69.simpleanimation.model.TeamResultModel;
import com.example.usyasha69.simpleanimation.ui.fragment.PreparatoryFragment;
import com.example.usyasha69.simpleanimation.ui.fragment.ResultFragment;
import com.example.usyasha69.simpleanimation.ui.fragment.GameFragment;

import java.util.ArrayDeque;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    public static final String TEAMS_SCORE_SHARED_PREFERENCES = "team_score_shared_preferences";

    private ArrayDeque<TeamModel> teamModelsQueue = new ArrayDeque<>();
    private ArrayList<TeamModel> teamModels = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        createCommands();

        createPreparatoryFragment();
    }

    public void createCommands() {
        SharedPreferences sharedPreferences = getSharedPreferences(TEAMS_SCORE_SHARED_PREFERENCES, Context.MODE_PRIVATE);
        int piratesScore = sharedPreferences.getInt("Pirates", 0);
        int camelotScore = sharedPreferences.getInt("Camelot", 0);

        teamModelsQueue.add(new TeamModel("Pirates", piratesScore));
        teamModelsQueue.add(new TeamModel("Camelot", camelotScore));

        teamModels.add(new TeamModel("Pirates", piratesScore));
        teamModels.add(new TeamModel("Camelot", camelotScore));
    }

    public void createGameFragment(TeamModel teamModel) {
                getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.am_container, GameFragment.newInstance(teamModel), GameFragment.class.getSimpleName())
                .commit();
    }

    public void createResultFragment(TeamResultModel teamResultModel) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.am_container, ResultFragment.newInstance(teamResultModel), ResultFragment.class.getSimpleName())
                .commit();
    }

    public void createPreparatoryFragment() {
        if (teamModelsQueue.isEmpty()) {
            finish();
            return;
        }

        SharedPreferences sharedPreferences = getSharedPreferences(TEAMS_SCORE_SHARED_PREFERENCES, Context.MODE_PRIVATE);
        int piratesScore = sharedPreferences.getInt("Pirates", 0);
        int camelotScore = sharedPreferences.getInt("Camelot", 0);

        TeamModel currentTeamModel = null;

        for (TeamModel teamModel : teamModelsQueue) {
            if (teamModel.name.equals("Pirates")) {
                currentTeamModel = teamModelsQueue.peek();
                currentTeamModel.score = piratesScore;
                break;
            } else if (teamModel.name.equals("Camelot")) {
                currentTeamModel = teamModelsQueue.peek();
                currentTeamModel.score = camelotScore;
                break;
            }
        }

        teamModelsQueue.poll();

        for (int i = 0; i < teamModels.size(); i++) {
            if (teamModels.get(i).name.equals("Pirates")) {
                teamModels.remove(teamModels.get(i));
                teamModels.add(i, new TeamModel("Pirates", piratesScore));
            } else if (teamModels.get(i).name.equals("Camelot")) {
                teamModels.remove(teamModels.get(i));
                teamModels.add(i, new TeamModel("Camelot", camelotScore));
            }
        }

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.am_container, PreparatoryFragment.newInstance(teamModels, currentTeamModel, 1, 1), PreparatoryFragment.class.getSimpleName())
                .commit();
    }
}
