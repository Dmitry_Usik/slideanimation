package com.example.usyasha69.simpleanimation.ui.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.usyasha69.simpleanimation.R;
import com.example.usyasha69.simpleanimation.model.TeamModel;
import com.example.usyasha69.simpleanimation.ui.activity.MainActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PreparatoryFragment extends Fragment {
    private static final String COMMAND_MODELS_BUNDLE_KEY = "command_models";
    private static final String COMMAND_MODEL_BUNDLE_KEY = "command_model";
    private static final String ROUND_BUNDLE_KEY = "round";
    private static final String GAME_BUNDLE_KEY = "game";

    @BindView(R.id.fp_first_team_name)
    TextView firstTeamName;
    @BindView(R.id.fp_first_team_score)
    TextView firstTeamScore;
    @BindView(R.id.fr_second_team_name)
    TextView secondTeamName;
    @BindView(R.id.fp_second_team_score)
    TextView secondTeamScore;
    @BindView(R.id.fp_numberOfRoundAndGame)
    TextView numberOfRoundAndGame;
    @BindView(R.id.fp_current_team_name)
    TextView currentTeamName;

    private ArrayList<TeamModel> teamModels;
    private TeamModel teamModel;
    private int round;
    private int game;

    public static PreparatoryFragment newInstance(ArrayList<TeamModel> teamModels
            , TeamModel teamModel, int round, int game) {
        Bundle args = new Bundle();
        args.putParcelableArrayList(COMMAND_MODELS_BUNDLE_KEY, teamModels);
        args.putParcelable(COMMAND_MODEL_BUNDLE_KEY, teamModel);
        args.putInt(ROUND_BUNDLE_KEY, round);
        args.putInt(GAME_BUNDLE_KEY, game);

        PreparatoryFragment fragment = new PreparatoryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        if (args != null && args.containsKey(COMMAND_MODEL_BUNDLE_KEY)
                && args.containsKey(COMMAND_MODELS_BUNDLE_KEY)
                && args.containsKey(ROUND_BUNDLE_KEY)
                && args.containsKey(GAME_BUNDLE_KEY)) {
            teamModels = args.getParcelableArrayList(COMMAND_MODELS_BUNDLE_KEY);
            teamModel = args.getParcelable(COMMAND_MODEL_BUNDLE_KEY);
            round = args.getInt(ROUND_BUNDLE_KEY);
            game = args.getInt(GAME_BUNDLE_KEY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_preparatory, container, false);
        ButterKnife.bind(this, root);

        firstTeamName.setText(teamModels.get(0).name);
        firstTeamScore.setText(String.valueOf(teamModels.get(0).score));
        secondTeamName.setText(teamModels.get(1).name);
        secondTeamScore.setText(String.valueOf(teamModels.get(1).score));

        numberOfRoundAndGame.setText("Round " + round + "\\" + " Game " + game);
        currentTeamName.setText(teamModel.name);

        return root;
    }

    @OnClick(R.id.fg_go)
    public void goGame(View v) {
        ((MainActivity) getActivity()).createGameFragment(teamModel);
    }
}
