package com.example.usyasha69.simpleanimation.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.LinkedHashMap;

public class TeamResultModel implements Parcelable {
    public String name;
    public int score;
    public LinkedHashMap<String, Boolean> wordsStatus;

    public TeamResultModel(String name, int score, LinkedHashMap<String, Boolean> wordsStatus) {
        this.name = name;
        this.score = score;
        this.wordsStatus = wordsStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TeamResultModel that = (TeamResultModel) o;

        if (score != that.score) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        return wordsStatus != null ? wordsStatus.equals(that.wordsStatus) : that.wordsStatus == null;

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + score;
        result = 31 * result + (wordsStatus != null ? wordsStatus.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "TeamResultModel{" +
                "name='" + name + '\'' +
                ", score=" + score +
                ", wordsStatus=" + wordsStatus +
                '}';
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeInt(this.score);
        dest.writeSerializable(this.wordsStatus);
    }

    protected TeamResultModel(Parcel in) {
        this.name = in.readString();
        this.score = in.readInt();
        this.wordsStatus = (LinkedHashMap<String, Boolean>) in.readSerializable();
    }

    public static final Parcelable.Creator<TeamResultModel> CREATOR = new Parcelable.Creator<TeamResultModel>() {
        @Override
        public TeamResultModel createFromParcel(Parcel source) {
            return new TeamResultModel(source);
        }

        @Override
        public TeamResultModel[] newArray(int size) {
            return new TeamResultModel[size];
        }
    };
}
