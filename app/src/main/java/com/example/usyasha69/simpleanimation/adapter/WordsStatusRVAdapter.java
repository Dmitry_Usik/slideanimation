package com.example.usyasha69.simpleanimation.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.usyasha69.simpleanimation.R;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WordsStatusRVAdapter extends RecyclerView.Adapter<WordsStatusRVAdapter.ViewHolder> {
    private Context context;
    private ArrayList<String> words;
    private ArrayList<Boolean> status;

    public WordsStatusRVAdapter(Context context, LinkedHashMap<String, Boolean> wordsStatus) {
        this.context = context;
        words = new ArrayList<>(wordsStatus.keySet());
        status = new ArrayList<>(wordsStatus.values());
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = layoutInflater.inflate(R.layout.words_status_rv, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.word.setText(words.get(position));

        Drawable wordStatus;

        if (status.get(position)) {
            wordStatus = ContextCompat.getDrawable(context, R.drawable.ic_mood_black_24dp);
        } else {
            wordStatus = ContextCompat.getDrawable(context, R.drawable.ic_mood_bad_black_24dp);
        }

        holder.wordStatus.setImageDrawable(wordStatus);
    }

    @Override
    public int getItemCount() {
        return words.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.gwrv_word)
        TextView word;
        @BindView(R.id.gwrv_word_status)
        ImageView wordStatus;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
