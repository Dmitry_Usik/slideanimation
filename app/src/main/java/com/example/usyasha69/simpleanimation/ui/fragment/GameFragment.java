package com.example.usyasha69.simpleanimation.ui.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.usyasha69.simpleanimation.R;
import com.example.usyasha69.simpleanimation.animation.AnimatorEventListener;
import com.example.usyasha69.simpleanimation.display.DisplayManager;
import com.example.usyasha69.simpleanimation.model.TeamModel;
import com.example.usyasha69.simpleanimation.model.TeamResultModel;
import com.example.usyasha69.simpleanimation.test.Mock;
import com.example.usyasha69.simpleanimation.ui.activity.MainActivity;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;

public class GameFragment extends Fragment {
    private static final long TIMER_TIME_MILLISECONDS = 120 * 1000;
    private static final long TIMER_TICK_INTERVAL_MILLISECONDS = 1000;
    private static final long MAIN_ANIMATION_DURATION = 400;
    private static final long BACK_TO_CENTER_ANIMATION_DURATION = 200;
    private static final long SWAP_WORD_DELAY = 390;

    private static final int UP_ANIMATION_FLAG = 0;
    private static final int BOTTOM_ANIMATION_FLAG = 1;

    private static final String TEAM_MODEL_BUNDLE_KEY = "team_model";

    @BindView(R.id.fg_circle)
    TextView circle;
    @BindView(R.id.fg_command_name)
    TextView commandName;
    @BindView(R.id.fg_guessed)
    TextView guessed;
    @BindView(R.id.fg_passed)
    TextView passed;
    @BindView(R.id.fg_timer)
    TextView timer;
    @BindView(R.id.fg_stop_game)
    Button stopGame;

    private CountDownTimer countDownTimer;
    private long timerValue = TIMER_TIME_MILLISECONDS;

    private GestureDetector gestureDetector;

    private TeamModel teamModel;
    private int guessedCount;
    private int passedCount;
    private boolean isGame;
    private boolean isClickToCircle;

    private float pressPoint;
    private boolean isCatched;

    private int statusBarHeight;
    private int middleOfDisplayHeightWithoutStatusBar;
    private int upPointOfStartAnimation;
    private int bottomPointOfStartAnimation;

    private ArrayList<String> words;
    private String currentWord;
    private LinkedHashMap<String, Boolean> wordsStatus;

    public static GameFragment newInstance(TeamModel teamModel) {
        Bundle args = new Bundle();
        args.putParcelable(TEAM_MODEL_BUNDLE_KEY, teamModel);

        GameFragment fragment = new GameFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();

        if (args != null && args.containsKey(TEAM_MODEL_BUNDLE_KEY)) {
            teamModel = args.getParcelable(TEAM_MODEL_BUNDLE_KEY);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_game, container, false);
        ButterKnife.bind(this, root);

        DisplayManager displayManager = new DisplayManager(getActivity().getWindowManager(), getResources());
        statusBarHeight = displayManager.getStatusBarHeight();
        middleOfDisplayHeightWithoutStatusBar = displayManager.getMiddleOfDisplayHeightWithoutStatusBar();
        upPointOfStartAnimation = displayManager.getUpPointOfStartAnimation();
        bottomPointOfStartAnimation = displayManager.getBottomPointOfStartAnimation();

        commandName.setText(teamModel.name);
        configureStopGameButton(false, View.INVISIBLE);

        words = Mock.getWords();
        wordsStatus = new LinkedHashMap<>();

        gestureDetector = new GestureDetector(getContext(), new SingleTapConfirm());

        return root;
    }

    @OnTouch(R.id.fg_circle)
    public boolean onTouchCircle(View v, MotionEvent event) {
        if (gestureDetector.onTouchEvent(event)) {
            startGame();
        }

        if (!isGame) {
            return true;
        }

        float eventRawY = event.getRawY();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                pressPoint = event.getY();
                isCatched = true;
                break;
            case MotionEvent.ACTION_MOVE:
                if (isCatched) {
                    if (circle.getY() < upPointOfStartAnimation) {
                        guessedAction(eventRawY);
                    } else if (circle.getY() + circle.getHeight() > bottomPointOfStartAnimation) {
                        passedAction(eventRawY);
                    } else {
                        circle.setY(eventRawY - pressPoint - statusBarHeight);
                    }
                }
                break;
            case MotionEvent.ACTION_CANCEL:
                if (isCatched) {
                    isCatched = false;
                    backToCentreCircleAnimation();
                }
                break;
            case MotionEvent.ACTION_UP:
                if (isCatched) {
                    isCatched = false;
                    backToCentreCircleAnimation();
                }
                break;
        }

        return true;
    }

    @OnClick(R.id.fg_stop_game)
    public void onClickStopGame(View v) {
        configureStopGameButton(false, View.INVISIBLE);

        circle.setText(getString(R.string.fr_continue));
        circle.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.blue_circle));

        isClickToCircle = false;
        isGame = false;

        countDownTimer.cancel();
    }

    private void configureStopGameButton(boolean clickable, int visibleMode) {
        stopGame.setClickable(clickable);
        stopGame.setVisibility(visibleMode);
    }

    private void guessedAction(float eventRawY) {
        isCatched = false;
        circle.setY(eventRawY - pressPoint - statusBarHeight);
        mainCircleAnimation(UP_ANIMATION_FLAG);

        guessedCount++;
        guessed.postDelayed(new Runnable() {
            @Override
            public void run() {
                swapCircleWord();
            }
        }, SWAP_WORD_DELAY);
        guessed.setText(String.valueOf(guessedCount));
        wordsStatus.put(currentWord, true);
    }

    public void passedAction(float eventRawY) {
        isCatched = false;
        circle.setY(eventRawY - pressPoint - statusBarHeight);
        mainCircleAnimation(BOTTOM_ANIMATION_FLAG);

        passedCount++;
        passed.setText(String.valueOf(passedCount));
        passed.postDelayed(new Runnable() {
            @Override
            public void run() {
                swapCircleWord();
            }
        }, SWAP_WORD_DELAY);
        wordsStatus.put(currentWord, false);
    }

    private void startTimer() {
        countDownTimer = new CountDownTimer(timerValue, TIMER_TICK_INTERVAL_MILLISECONDS) {
            @Override
            public void onTick(long millisUntilFinished) {
                timerValue = millisUntilFinished;
                timer.setText(formatMillisecondsToTime(millisUntilFinished));
            }

            @Override
            public void onFinish() {
                timer.setText(formatMillisecondsToTime(0));
                isGame = false;

                stopGame();
            }
        }.start();
    }

    private void startGame() {
        if (!isClickToCircle) {
            isClickToCircle = true;
            isGame = true;

            circle.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.grey_circle));
            configureStopGameButton(true, View.VISIBLE);
            swapCircleWord();

            startTimer();
        }
    }

    private void stopGame() {
        saveTeamScore();
        ((MainActivity) getActivity()).createResultFragment(new TeamResultModel(teamModel.name, guessedCount - passedCount, wordsStatus));
    }

    private void mainCircleAnimation(int animationFlag) {
        int translationY = 0;

        switch (animationFlag) {
            case UP_ANIMATION_FLAG:
                translationY = upPointOfStartAnimation;
                break;
            case BOTTOM_ANIMATION_FLAG:
                translationY = bottomPointOfStartAnimation;
                break;
        }

        circle.animate()
                .scaleX(0.0f)
                .scaleY(0.0f)
                .alpha(0.0f)
                .translationY(translationY - middleOfDisplayHeightWithoutStatusBar)
                .setDuration(MAIN_ANIMATION_DURATION)
                .setListener(new AnimatorEventListener(circle) {
                    @Override
                    public void onEnd() {
                        restoreCircleAnimation();
                    }
                }).start();
    }

    private void restoreCircleAnimation() {
        circle.animate()
                .scaleX(1.0f)
                .scaleY(1.0f)
                .alpha(1.0f)
                .translationY(0.0f)
                .setDuration(0)
                .setListener(new AnimatorEventListener(circle)).start();
    }

    private void backToCentreCircleAnimation() {
        circle.animate()
                .translationY(0.0f)
                .setDuration(BACK_TO_CENTER_ANIMATION_DURATION)
                .setListener(new AnimatorEventListener(circle)).start();
    }

    private String formatMillisecondsToTime(long milliseconds) {
        return String.format(Locale.getDefault(), "%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(milliseconds) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(milliseconds)),
                TimeUnit.MILLISECONDS.toSeconds(milliseconds) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(milliseconds)));
    }

    private void saveTeamScore() {
        SharedPreferences sharedPreferences = getContext().getSharedPreferences(MainActivity.TEAMS_SCORE_SHARED_PREFERENCES, Context.MODE_PRIVATE);

        sharedPreferences
                .edit()
                .putInt(teamModel.name, teamModel.score + guessedCount - passedCount)
                .apply();
    }

    private void swapCircleWord() {
        int randomIndex = (int) (Math.random() * words.size());
        currentWord = words.get(randomIndex);

        circle.setText(currentWord);
    }

    private class SingleTapConfirm extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onSingleTapUp(MotionEvent event) {
            return true;
        }
    }
}
