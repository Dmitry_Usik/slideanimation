package com.example.usyasha69.simpleanimation.display;

import android.content.res.Resources;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;

public class DisplayManager {
    private static final String DISPLAY_MANAGER_LOG_TAG = "DisplayManager";

    private WindowManager windowManager;
    private Resources resources;

    public DisplayManager(WindowManager windowManager, Resources resources) {
        this.windowManager = windowManager;
        this.resources = resources;
    }

    public int getDisplayHeight() {
        Log.d(DISPLAY_MANAGER_LOG_TAG, "getDisplayHeight()");

        int displayHeight = 0;

        DisplayMetrics displayMetrics = new DisplayMetrics();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            windowManager.getDefaultDisplay().getRealMetrics(displayMetrics);

            displayHeight = displayMetrics.heightPixels;
            Log.d(DISPLAY_MANAGER_LOG_TAG, "Display height = " + displayHeight + " px.");
        }

        return displayHeight;
    }

    public int getStatusBarHeight() {
        Log.d(DISPLAY_MANAGER_LOG_TAG, "getStatusBarHeight()");

        int statusBarHeight = 0;
        int resourceId = resources.getIdentifier("status_bar_height", "dimen", "android");

        if (resourceId > 0) {
            statusBarHeight = resources.getDimensionPixelSize(resourceId);
            Log.d(DISPLAY_MANAGER_LOG_TAG, "Status bar height = " + statusBarHeight + " px.");
        }

        return statusBarHeight;
    }

    public int getMiddleOfDisplayHeight() {
        Log.d(DISPLAY_MANAGER_LOG_TAG, "getMiddleOfDisplayHeight()");

        int middleOfDisplayHeight = getDisplayHeight() / 2;

        Log.d(DISPLAY_MANAGER_LOG_TAG, "Middle of display height = "
                + middleOfDisplayHeight + " px.");

        return middleOfDisplayHeight;
    }

    public int getMiddleOfDisplayHeightWithoutStatusBar() {
        Log.d(DISPLAY_MANAGER_LOG_TAG, "getMiddleOfDisplayHeightWithoutStatusBar()");

        int middleOfDisplayHeightWithoutStatusBar = (getDisplayHeight()  - getStatusBarHeight()) / 2;

        Log.d(DISPLAY_MANAGER_LOG_TAG, "Middle of display height without status bar = "
                + middleOfDisplayHeightWithoutStatusBar + " px.");

        return middleOfDisplayHeightWithoutStatusBar;
    }

    public int getUpPointOfStartAnimation() {
        Log.d(DISPLAY_MANAGER_LOG_TAG, "getUpPointOfStartAnimation()");

        int upPointOfStartAnimation = (getMiddleOfDisplayHeightWithoutStatusBar() / 4)
                + getMiddleOfDisplayHeightWithoutStatusBar() / 16;

        Log.d(DISPLAY_MANAGER_LOG_TAG, "Up point of start animation = "
                + upPointOfStartAnimation + " px.");

        return upPointOfStartAnimation;
    }

    public int getBottomPointOfStartAnimation() {
        Log.d(DISPLAY_MANAGER_LOG_TAG, "getBottomPointOfStartAnimation()");

        int bottomPointOfStartAnimation = getDisplayHeight() - getStatusBarHeight()
                - (getMiddleOfDisplayHeightWithoutStatusBar() / 4)
                - (getMiddleOfDisplayHeightWithoutStatusBar() / 16);

        Log.d(DISPLAY_MANAGER_LOG_TAG, "Bottom point of start animation = "
                + bottomPointOfStartAnimation + " px.");

        return bottomPointOfStartAnimation;
    }
}
